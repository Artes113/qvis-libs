var myUtils = {};
myUtils.getLanguage = function (txt) {
    var regexp = /[а-яА-Я]/g;
    if(txt.match(regexp)) {
        return "russian"
    }else{
        return "english"
    }
}

module.exports = myUtils;