var format = require("string-template")

var stats = {ru:{},en:{}}
stats.ru.BCnewVersion = 'Визитка <a href="https://flybc.co/{bc_id}">{bc_name}</a> была обновлена. Новая версия - <b>{version}</b>'
stats.en.BCnewVersion = 'The business card <a href="https://flybc.co/{bc_id}">{bc_name}</a> was updated. A new version - <b>{version}</b>'
stats.en.BCnewVersionForPush = 'The business card <b>{bc_name}</b> was updated'
stats.en.BCnewVersionForPush = 'The business card <b>{bc_name}</b> was updated'
stats.ru.BCardStoredOne = 'Визитка <a href="https://flybc.co/{bc_id}">{bc_name}</a> была сохранена в визитницу'
stats.en.BCardStoredOne = 'The business card <a href="https://flybc.co/{bc_id}">{bc_name}</a> was stored'
stats.ru.BCardsAfterPBookSync = 'После синхронизации с телефоной книгой мы добавили вам <b>{bcards_amoun}</b> визиток'
stats.en.BCardsAfterPBookSync = 'After synchronization with your phone book, we have added <b>{bcards_amoun}</b> business cards for you'
stats.ru.BCardAddedByContacts = 'Мы автоматически добавили вам визитку <a href="https://flybc.co/{bc_id}">{bc_name}</a> на основе вашей телефонной книги'
stats.en.BCardAddedByContacts = 'We have automated stored the business card <a href="https://flybc.co/{bc_id}">{bc_name}</a>, based on you contacts book'
stats.ru.ReferralRewards = 'Вы получили вознаграждение по реферальной системе:\r\n{rewards}'
stats.ru.ReferralRewardsNV = 'Новых версий:{count}\r\n'
stats.ru.ReferralRewardsBC = 'Визиток:{count}\r\n'
stats.ru.ReferralRewardsSC = 'Постоянный шаринг код:{count}\r\n'
stats.en.ReferralRewards = "You've received rewards on the referral system:\r\n{rewards}"
stats.en.ReferralRewardsNV = "New version:{count}\r\n"
stats.en.ReferralRewardsBC = "New business card:{count}\r\n"
stats.en.ReferralRewardsSC = "Persist sharing code:{count}\r\n"


var messages = function MessageBuilder(){
    this.locale="en"

    this.BCnewVersion=(bc_name, version, bc_id )=>{
        return  format(stats[this.locale].BCnewVersion, {bc_name: bc_name,version: version, bc_id:bc_id})
    }
    this.BCnewVersionForPush=(bc_name )=>{
        return  format(stats[this.locale].BCnewVersionForPush, {bc_name: bc_name})
    }

    this.BCardStoredOne=function(bc_name){
        return format(stats[this.locale].BCardStoredOne, {bc_name: bc_name})
    }

    this.BCardsAfterPBookSync = (bcards_amoun)=>{
        return    format(stats[this.locale].BCardsAfterPBookSync, {bcards_amoun: bcards_amoun})
    }

    this.BCardAddedByContacts = (bc_name, bc_id)=>{
        return  format(stats[this.locale].BCardAddedByContacts, {bc_name: bc_name, bc_id:bc_id})
    }

    this.ReferralRewards = (rw)=>{
        let rewards = ""
        if(rw.nv){rewards = format(stats[this.locale].ReferralRewardsNV, {count:rw.nv})}
        if(rw.bc){rewards = rewards + format(stats[this.locale].ReferralRewardsBC, {count:rw.bc})}
        if(rw.sc){rewards = rewards + format(stats[this.locale].ReferralRewardsSC, {count:rw.sc})}
        return  format(stats[this.locale].ReferralRewards, {rewards:rewards})
    }

}





Object.defineProperty(messages, "locale", {
    set: function(value) {
        if(typeof(newLocale) !== "string") {
            throw new Error("Locale must be a string");
        }
        if(value=="ru" || value=="en") {
            this.locale = value
        }else {
            throw new Error("Wrong locale");
        }
    }
});

module.exports = messages;