const Utils = require('./Utils');
const pgp = require('pg-promise')();

var dbGenerate = {};


dbGenerate.TRupdate = function (ths, tableName, fields, where) {
    var sql = "UPDATE " + tableName + " set ";

    for (var tname in fields) {
        if (fields[tname] === null) {
            continue
        }
        if (fields[tname] === 'decrement') {
            sql = sql + tname + "=" + tname + " - 1, "
        } else if (fields[tname] === 'increment') {
            sql = sql + tname + "=" + tname + " + 1, "
        } else if (typeof fields[tname] === "object" && !Array.isArray(fields[tname])) {
            if (fields[tname] && fields[tname].to_tsvector) {
                var lang = Utils.getLanguage(fields[tname].to_tsvector)
                sql = sql + " " + tname + "= to_tsvector('" + lang + "', ${" + tname + "}) || " + tname + ", "
                fields[tname] = fields[tname].to_tsvector
                continue
            }
            if (fields[tname].action == 'plus') {
                sql = sql + tname + "=" + tname + " + ${" + tname + "}, "
            } else if (fields[tname].action == 'minus') {
                sql = sql + tname + "=array_remove(" + tname + ", ${" + tname + "}), "
            } else if (fields[tname].action == 'arr_push') {
                sql = sql + tname + " = " + tname + " || ${" + tname + "}, "
            } else if (!fields[tname].value) {
                sql = sql + tname + " = ${" + tname + "}, "
            }

            if (fields[tname].value) {
                fields[tname] = fields[tname].value
            } else {
                fields[tname] = fields[tname]
            }

        } else {
            if (Array.isArray(fields[tname])) {
                sql = sql + tname + "=${" + tname + ":json}, "
            } else {
                sql = sql + tname + "=${" + tname + "}, "
            }

            // if (Array.isArray(fields[tname]) && typeof fields[tname][0] !== "object" && !fields[tname].is_json) {
            //     sql = sql + tname + "=${" + tname + "}::text[], "
            // } else if (Array.isArray(fields[tname]) && typeof fields[tname] === "object") {
            //     sql = sql + tname + "=${" + tname + ":json}, "
            // } else {
            //     sql = sql + tname + "=${" + tname + "}, "
            // }
        }
    }
    sql = sql.substring(0, sql.length - 2)
    sql = sql + " where "
    for (var tname in where) {
        fields[tname] = where[tname]
        if (Array.isArray(where[tname])) {
            const type = typeof where[tname][0] === "number" ? "int" : "text"
            sql = sql + tname + " IN (select(unnest(${" + tname + "}::" + type + "[]))) and ";
        } else {
            sql = sql + tname + "=${" + tname + "} and ";
        }
    }
    sql = sql.substring(0, sql.length - 5)
    return ths.any(sql, fields)
}

/*
 fields = {fieldName:value}
 fields = [{fieldName:value}]
 options{
    returning: [""],
    on_conflict: {
        action:"nothing",
        action:"update",
        set_fields:["name", "surname"],
    }
    }
*/
dbGenerate.TRinsert = function (ths, tableName, fields, options) {

    if (typeof options == "string") {
        const tmp = options
        options = {returning: [tmp]}
    }
    if (Array.isArray(options)) {
        const tmp = options
        options = {returning: tmp}
    }

    var sql = "INSERT INTO " + tableName;

    var fnames = " (";
    var fvals = " ("
    let isFnamesAdded = false;
    const fieldsForInsert = {}
    if (Array.isArray(fields)) {
        for (var i in fields) {
            for (var tname in fields[i]) {
                if (!isFnamesAdded) {
                    fnames = fnames + tname + ","
                }
                fvals = createInsertFields(tname, fields[i], fvals, fieldsForInsert, "a" + i);
            }
            isFnamesAdded = true;
            fvals = fvals.substring(0, fvals.length - 1) + "),("
        }
        fvals = fvals.substring(0, fvals.length - 2)
    } else {
        for (var tname in fields) {
            fnames = fnames + tname + ",";
            fieldsForInsert[tname] = fields[tname];
            fvals = createInsertFields(tname, fields, fvals, fieldsForInsert, "a");
        }
        fvals = fvals.substring(0, fvals.length - 1) + ")"
    }

    fnames = fnames.substring(0, fnames.length - 1) + ")"

    sql = sql + fnames + " VALUES " + fvals

    if (options && options.on_conflict == "nothing") {
        sql = sql + " on conflict do nothing "
    } else if (options && options.on_conflict == "update") {
        sql = sql + " on conflict do update set "
        for (const set_field in set_fields) {
            sql = sql + set_field + "=EXCLUDED." + set_field + ", "
        }
        sql = sql.substring(0, sql.length - 2)
    }


    if (options && options.returning) {
        sql = sql + " RETURNING " + options.returning
    }

    return ths.any(sql, fieldsForInsert)
}

function createInsertFields(tname, fields, fvals, fieldsForInsert, index) {

    if (fields[tname] && fields[tname].to_tsvector) {
        var lang = Utils.getLanguage(fields[tname].to_tsvector)
        fvals = fvals + "to_tsvector('" + lang + "', ${" + index + "_" + tname + "}),"
        fieldsForInsert[index + "_" + tname] = fields[tname].to_tsvector
    } else if (fields[tname] && fields[tname].currval) {
        fvals = fvals + "currval('" + fields[tname].currval + "'),"
        fieldsForInsert[index + "_" + tname] = fields[tname].currval
    } else if (fields[tname] && typeof fields[tname] === "object") {
        fvals = fvals + "${" + index + "_" + tname + ":json},"
        fieldsForInsert[index + "_" + tname] = fields[tname]
    } else {
        fvals = fvals + "${" + index + "_" + tname + "},"
        fieldsForInsert[index + "_" + tname] = fields[tname]
    }


    return fvals;
}

dbGenerate.TRinsertMulti = function (ths, tableName, values) {
    let cs = new pgp.helpers.ColumnSet(Object.keys(values[0]), {table: tableName});
// data input values:
//         const values = [{col_a: 'a1', col_b: 'b1'}, {col_a: 'a2', col_b: 'b2'}];
// generating a multi-row insert query:
    let query = pgp.helpers.insert(values, cs);
//=> INSERT INTO "tmp"("col_a","col_b") VALUES('a1','b1'),('a2','b2')
// executing the query:
    return ths.any(query)

}


dbGenerate.TRupsert = function (ths, tableName, fields, where) {
    let sql = "INSERT INTO " + tableName;
    let fnames = " (";
    let fvals = " ("
    for (var tname in fields) {
        fnames = fnames + tname + ","
        if (fields[tname] && fields[tname].to_tsvector) {
            let lang = Utils.getLanguage(fields[tname].to_tsvector)
            fvals = fvals + "to_tsvector('" + lang + "', ${" + tname + "}),"
            fields[tname] = fields[tname].to_tsvector
        } else {
            fvals = fvals + "${" + tname + "},"
        }
    }
    fnames = fnames.substring(0, fnames.length - 1) + ")"
    fvals = fvals.substring(0, fvals.length - 1) + ")"
    sql = sql + fnames + " VALUES " + fvals
    sql = sql + " on conflict ( "

    for (let tname in where) {
        sql = sql + where[tname] + ", ";
    }
    sql = sql.substring(0, sql.length - 2)
    sql = sql + " ) do update set "

    for (let tname in fields) {
        if (where.includes(tname)) {
            continue
        }

        if (fields[tname] === 'decrement') {
            sql = sql + tname + "=" + tname + " - 1, "
        } else if (fields[tname] === 'increment') {
            sql = sql + tname + "=" + tname + " + 1, "
        } else if (typeof fields[tname] === "object" && !Array.isArray(fields[tname])) {
            if (fields[tname] && fields[tname].to_tsvector) {
                let lang = Utils.getLanguage(fields[tname].to_tsvector)
                sql = sql + " " + tname + "= to_tsvector('" + lang + "', ${" + tname + "}) || " + tname + ", "
                fields[tname] = fields[tname].to_tsvector
                continue
            }

            if (fields[tname].action == 'plus') {
                sql = sql + tname + "=" + tname + " + ${" + tname + "}, "
            } else if (fields[tname].action == 'minus') {
                sql = sql + tname + "=array_remove(" + tname + ", ${" + tname + "}), "
            } else if (fields[tname].action == 'arr_push') {
                sql = sql + tname + " = " + tname + " || ${" + tname + "}, "
            }
            fields[tname] = fields[tname].value
        } else {
            if (Array.isArray(fields[tname])) {
                sql = sql + tname + "=${" + tname + "}::text[], "
            } else {
                sql = sql + tname + "=${" + tname + "}, "
            }
        }
    }
    sql = sql.substring(0, sql.length - 2)
    // sql=sql + " where "
    // for (var tname in where){
    //         sql = sql + where[tname] + "=${" + where[tname] + "} and ";
    //     }
    // sql = sql.substring(0, sql.length-5)

    return ths.any(sql, fields)
}


dbGenerate.TRdelete = function (ths, tableName, where) {
    var sql = "DELETE FROM " + tableName + " WHERE ";
    for (var tname in where) {
        if (Array.isArray(where[tname])) {
            let arr_type = typeof where[tname][0] === "number" ? "int" : "text"
            sql = sql + tname + " IN (select(unnest(${" + tname + "}::" + arr_type + "[]))) and ";
        } else {
            sql = sql + tname + "=${" + tname + "} and ";
        }
    }
    sql = sql.substring(0, sql.length - 5)
    return ths.any(sql, where)
}


dbGenerate.insert = function (db, tableName, fields, options) {
    return dbGenerate.TRinsert(db, tableName, fields, options)
}

dbGenerate.update = function (db, tableName, fields, where, returning) {
    var sql = "UPDATE " + tableName + " set ";

    for (var tname in fields) {
        if (fields[tname] === 'decrement') {
            sql = sql + tname + "=" + tname + " - 1, "
        } else if (fields[tname] === 'increment') {
            sql = sql + tname + "=" + tname + " + 1, "
        } else if ((typeof fields[tname] != "boolean" && fields[tname]) && typeof fields[tname] === "object" && !Array.isArray(fields[tname])) {
            if (fields[tname].action == 'plus') {
                sql = sql + tname + "=" + tname + " + ${" + tname + "}, "
            } else if (fields[tname].action == 'minus') {
                sql = sql + tname + "=" + tname + " - ${" + tname + "}, "
            } else if (fields[tname].action == 'arr_push') {
                sql = sql + tname + " = " + tname + " || ${" + tname + "}, "
            } else if (!fields[tname].value) {
                sql = sql + tname + " = ${" + tname + "}, "
            }

            if (fields[tname].value) {
                fields[tname] = fields[tname].value
            } else {
                fields[tname] = fields[tname]
            }

        } else {
            sql = sql + tname + "=${" + tname + "}, "
        }
    }
    sql = sql.substring(0, sql.length - 2)
    sql = sql + " where "
    for (var tname in where) {
        if (Array.isArray(where[tname])) {
            sql = sql + tname + " IN (select(unnest(${" + tname + "}::text[]))) and ";
        } else {
            sql = sql + tname + "=${" + tname + "} and ";
        }
        fields[tname] = where[tname]
    }
    sql = sql.substring(0, sql.length - 5)

    if (returning) {
        sql = sql + " RETURNING " + returning
    }

    return db.task(t => {
        // execute a chain of queries against the task context, and return the result:
        return t.any(sql, fields)
            .then(data => {
                return data;
            });
    })
}


dbGenerate.delete = function (db, tableName, where) {
    var sql = "DELETE FROM " + tableName + " WHERE ";
    for (var tname in where) {
        if (Array.isArray(where[tname])) {
            let arr_type = typeof where[tname][0] === "number" ? "int" : "text"
            sql = sql + tname + " IN (select(unnest(${" + tname + "}::" + arr_type + "[]))) and ";
        } else {
            sql = sql + tname + "=${" + tname + "} and ";
        }
    }
    sql = sql.substring(0, sql.length - 5)

    return db.task(t => {
        // execute a chain of queries against the task context, and return the result:
        return t.any(sql, where)
            .then(data => {
                return data;
            });
    })
}


// pagination : {page_number:2, perpage:30}
// order : {fieldName : ASC, fieldsName2: DESC}
// where : {
//          fieldName:"some value"
//          fieldName:["array of values, with condition IN"]
//          fieldName:{to_tsquery:"search text"}
//          fieldName:{like:"search text"}
//          fieldName:{value:["values of field"], condition:"NOT, =, !=, >, <"}
//          fieldName:{value:"some value", condition:"NOT, =, !=, >, <"}
//          fieldName:{between:{from:"2019-12-25", to:"2019-12-30"}}
//          }
// joins : {categories:{ pseudoName:"b",
//                      joinType:"left outer",
//                      table:"categories",
//                      fields_conf="all,manual,arr",
//                      fields:[{destTableField:"name", newName:"parentName"}],
//                      sourceField:"parent_id",
//                      destinationField:"id"
//                      where:{...the same as in parent...},
//                      }
//          },
//
//
dbGenerate.select = async function (db, tableNameF, fieldsF, whereF, orderF, paginationF, joinsF) {
    let tableName = tableNameF;
    let fields = fieldsF;
    let where = whereF;
    let order = orderF;
    let pagination = paginationF;
    let needCount = false;
    let joins = joinsF || [];
    let where_or = null;

    if (typeof tableNameF === "object") {
        tableName = tableNameF.tableName;
        fields = tableNameF.fields;
        where = tableNameF.where;
        order = tableNameF.order;
        needCount = tableNameF.needCount;
        pagination = tableNameF.pagination;
        joins = Object.entries(tableNameF.joins);
        where_or = tableNameF.whereOr;
    }

    const joinName = "a"
    const fieldsForPostgress = {}
    let sql = "SELECT "
    for (let field of fields) {
        sql = sql + joinName + "." + field + ", "
    }
    if (needCount) {
        sql = sql + " count(*) OVER() AS full_count "
    } else if (fields[0]) {
        sql = sql.substring(0, sql.length - 2)
    }

    for (const [key, join] of joins) {
        if (join.fields_conf == "all") {
            sql = sql + ", " + join.pseudoName + ".*"
        }
        if (join.fields_conf == "manual") {
            for (let field of join.fields) {
                if (field.destTableField) {
                    sql = sql + ", " + join.pseudoName + "." + field.destTableField + " " + field.newName
                } else {
                    sql = sql + ", " + join.pseudoName + "." + field + " " + field
                }
            }
        }


        if (join.fields_conf == "arr") {//fields:["id", "name", "created_date"]
            for (let field of join.fields) {
                sql = sql + ", " + join.pseudoName + "." + field
            }
        }
    }

    sql = sql + " FROM " + tableName + " " + joinName;
    for (const [key, join] of joins) {
        if (join.internalSql) {
            sql = sql + " " + join.joinType + " JOIN (" + join.internalSql + ") " + join.pseudoName +
                " ON " + joinName + "." + join.sourceField + " = " + join.pseudoName + "." + join.destinationField
        } else {
            sql = sql + " " + join.joinType + " JOIN " + join.tableName + " " + join.pseudoName +
                " ON " + joinName + "." + join.sourceField + " = " + join.pseudoName + "." + join.destinationField
        }
    }

    let isWhere = {isWhere: false}
    sql = sql + " where "

    sql = whereBuilder(sql, where, joinName, fieldsForPostgress, isWhere, where_or)
    for (const [key, join] of joins) {
        sql = whereBuilder(sql, join.where, join.pseudoName, fieldsForPostgress, isWhere)
    }
    if (!isWhere.isWhere) {
        sql = sql.substring(0, sql.length - 6)
    } else {
        sql = sql.substring(0, sql.length - 5)
    }

    if (order && Array.isArray(order)) {
        for (let orderfield of order) {
            for (let orderFieldId in orderfield) {
                sql = sql + " order by " + joinName + "." + orderFieldId + " " + orderfield[orderFieldId]
            }
        }
    } else if (order) {
        for (let orderFieldId in order) {
            sql = sql + " order by " + joinName + "." + orderFieldId + " " + order[orderFieldId]
        }
    }


    if (pagination && typeof pagination === "object") {
        let offset = (pagination.page_number - 1) * pagination.perpage
        sql = sql + " OFFSET " + offset + " limit " + pagination.perpage
    } else if (pagination) {
        sql = sql + " limit " + pagination
    }


    return await db.any(sql, fieldsForPostgress)
}

function whereBuilder(sql, where, tableIndex, fields, isWhere) {
    for (let tname in where) {
        isWhere.isWhere = true
        if (Array.isArray(where[tname]) && where[tname].length == 1) {
            where[tname] = where[tname][0]
        }

        if (typeof where[tname] === "object" && !Array.isArray(where[tname])) {
            if (where[tname] && where[tname].from && where[tname].to) {
                sql = sql + " " + tableIndex + "." + tname + " >= ${" + tableIndex + "_" + tname + "_from }::date and "
                sql = sql + " " + tableIndex + "." + tname + " <= ${" + tableIndex + "_" + tname + "_to}::date and "
                fields[tableIndex + "_" + tname + "_from"] = where[tname].from
                fields[tableIndex + "_" + tname + "_to"] = where[tname].to
                continue
            }

            if (where[tname] && where[tname].to_tsquery) {
                sql = sql + " " + tableIndex + "." + tname + " @@ to_tsquery(${" + tableIndex + "_" + tname + "}) and "
                fields[tableIndex + "_" + tname] = where[tname].to_tsquery
                continue
            }
            if (where[tname] && where[tname].like) {
                sql = sql + " " + tableIndex + "." + tname + " ILIKE ${" + tableIndex + "_" + tname + "} and "
                fields[tableIndex + "_" + tname] = "%" + where[tname].like + "%"
                continue
            }
            if (Array.isArray(where[tname].value)) {
                let type = "text"
                if (typeof where[tname][0] === "number") {
                    type = "int"
                }
                sql = sql + tableIndex + "." + tname + " " + where[tname].condition + " IN (select(unnest( ${" + tableIndex + "_" + tname + "}::" + type + "[]))) and ";
            } else {
                sql = sql + tableIndex + "." + tname + " " + where[tname].condition + " ${" + tableIndex + "_" + tname + "} and ";
            }
            fields[tableIndex + "_" + tname] = where[tname].value
            continue
        } else if (Array.isArray(where[tname])) {
            let type = "text"
            if (typeof where[tname][0] === "number") {
                type = "int"
            }
            sql = sql + tableIndex + "." + tname + " IN (select(unnest(${" + tableIndex + "_" + tname + "}::" + type + "[]))) and ";
        } else {
            sql = sql + tableIndex + "." + tname + "=${" + tableIndex + "_" + tname + "} and ";
        }
        fields[tableIndex + "_" + tname] = where[tname];
    }

    return sql;

}


module.exports = dbGenerate;
