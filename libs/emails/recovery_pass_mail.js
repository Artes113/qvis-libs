var format = require("string-template")


var template = '<div bgcolor="#FFFFFF" style="margin:0;padding:0;width:100%!important;height:100%!important">' +
    '   <div align="center">' +
    '      <table width="100%" border="0" cellspacing="0" cellpadding="0">' +
    '         <tbody>' +
    '            <tr>' +
    '               <td align="center" valign="top">' +
    '                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="m_7368999995062127386table700" style="min-width:400px;max-width:560px;border-collapse:separate">' +
    '                     <tbody>' +
    '                        <tr>' +
    '                           <td style="border-collapse:collapse">' +
    '                              <div style="height:16px;line-height:14px;font-size:12px">&nbsp;</div>' +
    '                           </td>' +
    '                        </tr>' +
    '                        <tr>' +
    '                           <td>' +
    '                              <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:separate">' +
    '                                 <tbody>' +
    '                                    <tr>' +
    '                                       <td style="border-collapse:separate;border-radius:4px;background:#ffffff;border:1px solid #e1e1e1" align="center" valign="top">' +
    '                                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="padding:0 45px;border-spacing:0">' +
    '                                             <tbody>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse;background:#ffffff">' +
    '                                                      <div style="height:22px;line-height:22px;font-size:20px">&nbsp;</div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                         <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0">' +
    '                                                            <tbody>' +
    '                                                               <tr>' +
    '                                                                  <td style="border-collapse:collapse">' +
    '                                                                     <a href="https://flybc.net/" style="font-family:Arial,sans-serif;border:none;font-size:18px;color:#ffffff;text-decoration:none!important" target="_blank">' +
    '                                                                     <img src="https://flybc.net/site/img/small_icon.png" border="0" style="display:block;border:none;outline:none;height: 50px;" class="CToWUd">' +
    '                                                                    ' +
    '                                                                     </a>' +
    '                                                                  </td>' +
    '                                                               </tr>' +
    '                                                            </tbody>' +
    '                                                         </table>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:39px;line-height:34px;font-size:34px">&nbsp;</div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse;text-align:center;">' +
    '                                                         <div>' +
    '                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%" align="center" style="border-spacing:0">' +
    '                                                               <tbody>' +
    '                                                                  <tr>' +
    '                                                                     <td style="font-size:16px;text-align:left;line-height:22px;font-family:\'Open Sans\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif;color:#000000;font-weight:lighter">' +
    '                                                                        Hi, {user_name}!' +
    '                                                                     </td>' +
    '                                                                  </tr>' +
    '                                                               </tbody>' +
    '                                                            </table>' +
    '                                                         </div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:39px;line-height:34px;font-size:34px">&nbsp;</div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse;text-align:center;">' +
    '                                                         <div>' +
    '                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%" align="center" style="border-spacing:0">' +
    '                                                               <tbody>' +
    '                                                                  <tr>' +
    '                                                                     <td style="font-size:16px;text-align:left;line-height:22px;font-family:\'Open Sans\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif;color:#000000;font-weight:lighter">' +
    'We heard that you need help. If you forgot your password, please follow this link to restore.' +
    '                                                                     </td>' +
    '                                                                  </tr>' +
    '                                                               </tbody>' +
    '                                                            </table>' +
    '                                                         </div>' +
    '                                                   </td>' +
    '                                                </tr>                                                ' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:30px;line-height:20px;font-size:20px"> &nbsp; </div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <center>' +
    '                                                         <table width="190" height="56" style="border-collapse:separate;border-radius:4px;background:#00bff3" align="center" border="0" cellspacing="0" cellpadding="0">' +
    '                                                            <tbody>' +
    '                                                               <tr>' +
    '                                                                  <td align="center" valign="middle" height="56">' +
    '                                                                     <a href="{recovery_link}" style="font-family:\'Open Sans\',Verdana,sans-serif;width:180px;line-height:56px;display:inline-block;font-size:14px;color:#ffffff;text-decoration:none;font-weight:bold" target="_blank" >' +
    '                                                                     <span style="font-size:16px;color:#ffffff;letter-spacing:0.5px">Restore pass link</span>' +
    '                                                                     </a>' +
    '                                                                  </td>' +
    '                                                               </tr>' +
    '                                                            </tbody>' +
    '                                                         </table>' +
    '                                                      </center>' +
    '                                                   </td>' +
    '                                                </tr>                                                ' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:30px;line-height:20px;font-size:20px"> &nbsp; </div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse;text-align:center;">' +
    '                                                         <div>' +
    '                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%" align="center" style="border-spacing:0">' +
    '                                                               <tbody>' +
    '                                                                  <tr>' +
    '                                                                     <td style="font-size:16px;text-align:left;line-height:22px;font-family:\'Open Sans\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif;color:#000000;font-weight:lighter">' +
    ' </td>' +
    '                                                                  </tr>' +
    '                                                               </tbody>' +
    '                                                            </table>' +
    '                                                         </div>' +
    '                                                   </td>' +
    '                                                </tr>                                                 ' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:30px;line-height:20px;font-size:20px"> &nbsp; </div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse;text-align:center;">' +
    '                                                         <div>' +
    '                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%" align="center" style="border-spacing:0">' +
    '                                                               <tbody>' +
    '                                                                  <tr>' +
    '                                                                     <td style="font-size:16px;text-align:left;line-height:22px;font-family:\'Open Sans\',\'HelveticaNeue-Light\',\'Helvetica Neue Light\',\'Helvetica Neue\',Helvetica,Arial,\'Lucida Grande\',sans-serif;color:#000000;font-weight:lighter">' +
    '                                                                        Thank you for using FlyCard!' +
    '                                                                     </td>' +
    '                                                                  </tr>' +
    '                                                               </tbody>' +
    '                                                            </table>' +
    '                                                         </div>' +
    '                                                   </td>' +
    '                                                </tr>     ' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:40px;line-height:40px;font-size:40px"> &nbsp; </div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                             </tbody>' +
    '                                          </table>' +
    '                                       </td>' +
    '                                    </tr>' +
    '                                 </tbody>' +
    '                              </table>' +
    '                           </td>' +
    '                        </tr>' +
    '                        <tr>' +
    '                           <td style="border-collapse:collapse;background:#ffffff">' +
    '                              <div style="height:22px;line-height:15px;font-size:15px">&nbsp;</div>' +
    '                           </td>' +
    '                        </tr>' +
    '                        <tr>' +
    '                          <td style="border-collapse:collapse">' +
    '                             <center>' +
    '                                <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0">' +
    '                                   <tbody>' +
    '                                      <tr>' +
    '                                         <td style="border-collapse:collapse" width="8">' +
    '                                            <div style="font-size:5px;height:5px;line-height:5px"> </div>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse">' +
    '                                            <table border="0" cellspacing="0" cellpadding="0">' +
    '                                               <tbody>' +
    '                                                  <tr>' +
    '                                                     <td valign="top">' +
    '                                                        <a href="https://twitter.com/FlyCard_Network" style="font-family:\'Open Sans\',Verdana,sans-serif;display:inline-block;font-size:14px;color:#3273a1;text-transform:uppercase;text-decoration:none" target="_blank" data-saferedirecturl="https://twitter.com/FlyCard_Network">' +
    '                                                        <img src="https://flybc.net/site/img/tw_icon.png" width="32" border="0" height="32" alt="" style="border:none;outline:none" class="CToWUd">' +
    '                                                        </a>' +
    '                                                     </td>' +
    '                                                  </tr>' +
    '                                               </tbody>' +
    '                                            </table>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse" width="8">' +
    '                                            <div style="font-size:5px;height:5px;line-height:5px"> </div>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse"></td>' +
    '                                         <td style="border-collapse:collapse">' +
    '                                            <table style="border-collapse:separate" border="0" cellspacing="0" cellpadding="0">' +
    '                                               <tbody>' +
    '                                                  <tr>' +
    '                                                     <td valign="top">' +
    '                                                        <a href="https://www.facebook.com/flybc.net" style="font-family:\'Open Sans\',Verdana,sans-serif;display:inline-block;font-size:14px;color:#3273a1;text-transform:uppercase;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=uk&amp;q=https://www.facebook.com/flybc.net">' +
    '                                                        <img src="https://flybc.net/site/img/fb_icon.png" width="32" border="0" height="32" alt="" style="border:none;outline:none" class="CToWUd">' +
    '                                                        </a>' +
    '                                                     </td>' +
    '                                                  </tr>' +
    '                                               </tbody>' +
    '                                            </table>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse" width="8">' +
    '                                            <div style="font-size:5px;height:5px;line-height:5px"> </div>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse">' +
    '                                            <table border="0" cellspacing="0" cellpadding="0">' +
    '                                               <tbody>' +
    '                                                  <tr>' +
    '                                                     <td valign="top">' +
    '                                                        <a href="https://vk.com/fly_card" style="font-family:\'Open Sans\',Verdana,sans-serif;display:inline-block;font-size:14px;color:#3273a1;text-transform:uppercase;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=uk&amp;q=https://vk.com/fly_card">' +
    '                                                        <img src="https://flybc.net/site/img/vk_icon.png" width="32" border="0" height="32" alt="" style="border:none;outline:none" class="CToWUd">' +
    '                                                        </a>' +
    '                                                     </td>' +
    '                                                  </tr>' +
    '                                               </tbody>' +
    '                                            </table>' +
    '                                         </td>' +
    '                                      </tr>' +
    '                                   </tbody>' +
    '                                </table>' +
    '                             </center>' +
    '                          </td>' +
    '                        </tr>' +
    '                        <tr>' +
    '                           <td style="border-collapse:collapse;background:#ffffff">' +
    '                              <div style="height:22px;line-height:15px;font-size:15px">&nbsp;</div>' +
    '                           </td>' +
    '                        </tr>' +
    '                     </tbody>' +
    '                  </table>' +
    '               </td>' +
    '            </tr>' +
    '         </tbody>' +
    '      </table>' +
    '   </div>' +
    '</div>'


module.exports = template