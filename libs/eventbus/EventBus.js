var eventBus = {};


eventBus.eventMaping = {}

eventBus.addListener = function (topic, listenerFunction) {
    if(!eventBus.eventMaping[topic]){eventBus.eventMaping[topic]=[]}
    eventBus.eventMaping[topic].push(listenerFunction)
}

eventBus.pushEvent= function(topic, event){
    for(var i in eventBus.eventMaping[topic]){
        eventBus.eventMaping[topic][i](event)
    }
}

module.exports = eventBus;
