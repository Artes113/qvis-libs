var nodemailer = require('nodemailer');
var pass_tmpl = require('./emails/recovery_pass_mail');
var thanks_tmplt = require('./emails/thanks_mail');
var format = require("string-template")
var mailer = {}
// create reusable transporter object using the default SMTP transport
var transporter = nodemailer.createTransport({direct:true,
    host: 'smtp.yandex.ru',
    port: 465,
    auth: {
        user: 'no-replay@qvis.mobi',
        pass: 'ZB32bmjo' },
    secure: true
});

mailer.sendEmail = function (to, subject, text, html) {

    // setup email data with unicode symbols
    var mailOptions = {
        from: '"FlyCard" <no-replay@flybc.net>', // sender address
        to: to, // list of receivers
        subject: subject, // Subject line
        text: text, // plain text body
        html: html // html body
    };

// send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            return console.log(error);
        }
        // console.log('Message %s sent: %s', info.messageId, info.response);
    });

}

mailer.sendRemindPass = function(to, pass_recovery_id, user_name){
    var body = format(pass_tmpl, {
        user_name: user_name,
        recovery_link: 'https://flybc.net/recovery/pass/' + pass_recovery_id
    })

    mailer.sendEmail(to, 'Password recovery', 'SDSAD',body)
}


mailer.sendMailAfterRegistration = function(to){
    var body = thanks_tmplt
    mailer.sendEmail(to, 'Thank you for registration', 'SDSAD', body)
}

mailer.sendMailAfterChangePassword = function(to){
    var body =  "<p>Hello, the password for your account was changed.</p>"+
        "<p>If it wasn't you please contact us support@flybc.net</p>"
    mailer.sendEmail(to, 'Password was changed', 'SDSAD', body)
}











module.exports = mailer