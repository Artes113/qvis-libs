

var template ='<div bgcolor="#FFFFFF" style="margin:0;padding:0;width:100%!important;height:100%!important">' +
    '   <div align="center">' +
    '      <table width="100%" border="0" cellspacing="0" cellpadding="0">' +
    '         <tbody>' +
    '            <tr>' +
    '               <td align="center" valign="top">' +
    '                  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="m_7368999995062127386table700" style="min-width:400px;max-width:560px;border-collapse:separate">' +
    '                     <tbody>' +
    '                        <tr>' +
    '                           <td style="border-collapse:collapse">' +
    '                              <div style="height:16px;line-height:14px;font-size:12px">&nbsp;</div>' +
    '                           </td>' +
    '                        </tr>' +
    '                        <tr>' +
    '                           <td>' +
    '                              <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-spacing:0;border-collapse:separate">' +
    '                                 <tbody>' +
    '                                    <tr>' +
    '                                       <td style="border-collapse:separate;border-radius:4px;background:#ffffff;border:1px solid #e1e1e1" align="center" valign="top">' +
    '                                          <table border="0" width="100%" cellpadding="0" cellspacing="0" style="border-spacing:0">' +
    '                                             <tbody>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:39px;line-height:34px;font-size:34px">&nbsp;</div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse;text-align:center;padding-left:20px;padding-right:20px">' +
    '                                                      <center>' +
    '                                                         <div style="max-width:390px;text-align:center;margin-right:auto;margin-left:auto">' +
    '                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%" align="center" style="border-spacing:0">' +
    '                                                               <tbody>' +
    '                                                                  <tr>' +
    '                                                                     <td style="font-size:30px;text-align:center;line-height:40px;font-family:\'Open Sans\',\'Segoe UI\',Roboto,Arial,sans-serif;color:#000000;font-weight:lighter">' +
    '                                                                        Благодарим вас за регистрацию в Fly Card.' +
    '                                                                     </td>' +
    '                                                                  </tr>' +
    '                                                               </tbody>' +
    '                                                            </table>' +
    '                                                         </div>' +
    '                                                      </center>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:30px;line-height:20px;font-size:20px"> &nbsp; </div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <center>' +
    '                                                         <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0">' +
    '                                                            <tbody>' +
    '                                                               <tr>' +
    '                                                                  <td style="border-collapse:collapse">' +
    '                                                                     <a href="https://flybc.net/" style="border:none;text-decoration:none!important" target="_blank" > <img alt="https://flybc.net/site/img/logo.png" src="https://flybc.net/site/img/qvis_mail_logo.png" class="CToWUd">' +
    '                                                                     </a>' +
    '                                                                  </td>' +
    '                                                               </tr>' +
    '                                                            </tbody>' +
    '                                                         </table>' +
    '                                                      </center>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:20px;line-height:22px;font-size:22px">&nbsp;</div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                  <td style="border-collapse:collapse;text-align:center;padding-left:20px;padding-right:20px">' +
    '                                                     <center>' +
    '                                                        <div style="max-width:390px;text-align:center;margin-right:auto;margin-left:auto">' +
    '                                                           <table cellspacing="0" cellpadding="0" border="0" width="100%" align="center" style="border-spacing:0">' +
    '                                                              <tbody>' +
    '                                                                 <tr>' +
    '                                                                    <td style="font-size:20px;text-align:center;line-height:40px;font-family:\'Open Sans\',\'Segoe UI\',Roboto,Arial,sans-serif;color:#000000;font-weight:lighter">' +
    '                                                                       Мы уверены, что это приложение действительно поможет Вам!' +
    '                                                                    </td>' +
    '                                                                 </tr>' +
    '                                                              </tbody>' +
    '                                                           </table>' +
    '                                                        </div>' +
    '                                                     </center>' +
    '                                                  </td>' +
    '                                                </tr>' +
    '                                                <tr>' +
    '                                                   <td style="border-collapse:collapse">' +
    '                                                      <div style="height:40px;line-height:40px;font-size:40px"> &nbsp; </div>' +
    '                                                   </td>' +
    '                                                </tr>' +
    '                                             </tbody>' +
    '                                          </table>' +
    '                                       </td>' +
    '                                    </tr>' +
    '                                 </tbody>' +
    '                              </table>' +
    '                           </td>' +
    '                        </tr>' +
    '                        <tr>' +
    '                           <td style="border-collapse:collapse;background:#ffffff">' +
    '                              <div style="height:22px;line-height:15px;font-size:15px">&nbsp;</div>' +
    '                           </td>' +
    '                        </tr>' +
    '                        <tr>' +
    '                          <td style="border-collapse:collapse">' +
    '                             <center>' +
    '                                <table border="0" cellpadding="0" cellspacing="0" style="border-spacing:0">' +
    '                                   <tbody>' +
    '                                      <tr>' +
    '                                         <td style="border-collapse:collapse" width="8">' +
    '                                            <div style="font-size:5px;height:5px;line-height:5px"> </div>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse">' +
    '                                            <table border="0" cellspacing="0" cellpadding="0">' +
    '                                               <tbody>' +
    '                                                  <tr>' +
    '                                                     <td valign="top">' +
    '                                                        <a href="https://twitter.com/FlyCard_Network" style="font-family:\'Open Sans\',Verdana,sans-serif;display:inline-block;font-size:14px;color:#3273a1;text-transform:uppercase;text-decoration:none" target="_blank" data-saferedirecturl="https://twitter.com/FlyCard_Network">' +
    '                                                        <img src="https://flybc.net/site/img/tw_icon.png" width="32" border="0" height="32" alt="" style="border:none;outline:none" class="CToWUd">' +
    '                                                        </a>' +
    '                                                     </td>' +
    '                                                  </tr>' +
    '                                               </tbody>' +
    '                                            </table>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse" width="8">' +
    '                                            <div style="font-size:5px;height:5px;line-height:5px"> </div>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse"></td>' +
    '                                         <td style="border-collapse:collapse">' +
    '                                            <table style="border-collapse:separate" border="0" cellspacing="0" cellpadding="0">' +
    '                                               <tbody>' +
    '                                                  <tr>' +
    '                                                     <td valign="top">' +
    '                                                        <a href="https://www.facebook.com/flybc.net" style="font-family:\'Open Sans\',Verdana,sans-serif;display:inline-block;font-size:14px;color:#3273a1;text-transform:uppercase;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=uk&amp;q=https://www.facebook.com/flybc.net">' +
    '                                                        <img src="https://flybc.net/site/img/fb_icon.png" width="32" border="0" height="32" alt="" style="border:none;outline:none" class="CToWUd">' +
    '                                                        </a>' +
    '                                                     </td>' +
    '                                                  </tr>' +
    '                                               </tbody>' +
    '                                            </table>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse" width="8">' +
    '                                            <div style="font-size:5px;height:5px;line-height:5px"> </div>' +
    '                                         </td>' +
    '                                         <td style="border-collapse:collapse">' +
    '                                            <table border="0" cellspacing="0" cellpadding="0">' +
    '                                               <tbody>' +
    '                                                  <tr>' +
    '                                                     <td valign="top">' +
    '                                                        <a href="https://vk.com/fly_card" style="font-family:\'Open Sans\',Verdana,sans-serif;display:inline-block;font-size:14px;color:#3273a1;text-transform:uppercase;text-decoration:none" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=uk&amp;q=https://vk.com/fly_card">' +
    '                                                        <img src="https://flybc.net/site/img/vk_icon.png" width="32" border="0" height="32" alt="" style="border:none;outline:none" class="CToWUd">' +
    '                                                        </a>' +
    '                                                     </td>' +
    '                                                  </tr>' +
    '                                               </tbody>' +
    '                                            </table>' +
    '                                         </td>' +
    '                                      </tr>' +
    '                                   </tbody>' +
    '                                </table>' +
    '                             </center>' +
    '                          </td>' +
    '                        </tr>' +
    '                        <tr>' +
    '                           <td style="border-collapse:collapse;background:#ffffff">' +
    '                              <div style="height:22px;line-height:15px;font-size:15px">&nbsp;</div>' +
    '                           </td>' +
    '                        </tr>' +
    '                     </tbody>' +
    '                  </table>' +
    '               </td>' +
    '            </tr>' +
    '         </tbody>' +
    '      </table>' +
    '   </div>' +
    '</div>'




module.exports = template