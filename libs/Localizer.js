var format = require("string-template")

var bucket = {ru:{}, en:{}}

bucket.ru.bcGetsNewVersion="Один из ваших контактов, создал себе публичную визитку - {bc_name}."
bucket.en.bcGetsNewVersion="One of your contact has created new public business card - {bc_name}"


bucket.ru.refObt="Вы привели к нам {count} пользователей, спасибо! За это мы подарили вам несколько падарков"
bucket.en.refObt="You brought us {count} users, thanks a lot! And we give you some gifts."

greeting = format("Hello {name}, you have {count} unread messages", {name: "Robert",count: 12})



bucket.format=format

module.exports = bucket;