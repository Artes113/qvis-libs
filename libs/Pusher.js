var gcm = require('node-gcm');
var sender = new gcm.Sender('AAAArCoZbUM:APA91bHuveK8_tdHBYEDbThRouhw7h4NMAZGOeOuR9rDH9pzaJFE6rlH-Ua8yXvCKGOZ1TnguCDhs_iV64Jze7-mzdruF8T0gNQqg34Bsui-bzvD6rSCQlMJ67rJ_EqN6Cv3iPOo6-gk');
var dbHelper = require("./DbHelper")

var apn = require('apn');
var db = null;
var options = {
    token: {
        key: __dirname+"/../AuthKey_7BDX8G5UZ6.p8",
        keyId: "7BDX8G5UZ6",
        teamId: "QZQNE4XQC4"
    },
    production: false
};

var apnProvider = new apn.Provider(options);

function pushToIosSilent(tokens, payload, call) {
    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    // note.contentAvailable = true // "ping.aiff";
    note.payload = payload // {'messageFrom': 'John Appleseed'};
    note.topic = "mobi.qvis.Qvis";
    note.badge = 0 // 5;
    note.sound = "" // "ping.aiff";
    note.alert = "" // "\uD83D\uDCE7 \u2709 You have a new message";
    // note.rawPayload = data.rawPayload // {'messageFrom': 'John Appleseed'};

    apnProvider.send(note, tokens).then( (result) => {

        var deprecated_tokens = []
        for (var t in result.failed) {
            var rt = result.failed[t].device

            deprecated_tokens.push(rt)

        }
        if (deprecated_tokens.length != 0) {
            dbHelper.delete(db, "user_devices", {push_token: deprecated_tokens})
        }

        console.log("send to tokens: \r\n" + tokens + "\r\ndeprecated tokens:\n" + deprecated_tokens + "\r\n apn response:\n" + JSON.stringify(result))

    });
}
function pushToIos(tokens, payload, alertMessage, call) {
    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
    note.payload = payload // {'messageFrom': 'John Appleseed'};
    note.topic = "mobi.qvis.Qvis";
    note.alert = alertMessage;
    // note.badge = data.badge // 5;
    // note.sound = data.sound // "ping.aiff";

    apnProvider.send(note, tokens).then( (result) => {

            var deprecated_tokens = []
            for (var t in result.failed) {
                var rt = result.failed[t].device

                    deprecated_tokens.push(rt)

            }
            if (deprecated_tokens.length != 0) {
                dbHelper.delete(db, "user_devices", {push_token: deprecated_tokens})
            }

        console.log("send to tokens: \r\n" + tokens + "\r\ndeprecated tokens:\n" + deprecated_tokens + "\r\n ggl response:\n" + JSON.stringify(response) + "\r\nresp error:\r\n" + err)

    });
}


function pushToAndroid(tokens, payload, call) {

    var message = new gcm.Message({
        // collapseKey: user_id + "syncBCHolder",
        priority: 'high',
        contentAvailable: true,
        delayWhileIdle: true,
        timeToLive: 3,
        data:payload
    });

    sender.send(message, {registrationTokens: tokens}, function (err, response) {
        if (err) console.error(err);
        else {
            var deprecated_tokens = []
            for (var t in response.results) {
                var rt = response.results[t]
                if (rt.error) {
                    deprecated_tokens.push(tokens[t])
                }
            }
            if (deprecated_tokens.length != 0) {
                dbHelper.delete(db, "user_devices", {push_token: deprecated_tokens})
            }
        }
        console.log("send to tokens: \r\n" + tokens + "\r\ndeprecated tokens:\n" + deprecated_tokens + "\r\n ggl response:\n" + JSON.stringify(response) + "\r\nresp error:\r\n" + err)
    });

}

function Pusher(db1){
db=db1;


    this.pushToUser = function(user_id, payload){
        if(!payload.action){
            console.error("Wrong actions",payload)
        }
        dbHelper.select(db, "user_devices", ["*"], {user_id:user_id}).then((data)=>{
            var android_tokens = []
            var ios_tokens = []
            var isEmptyIos = true
            var isEmptyAndr = true
            for (var i in data) {
                var token = data[i].push_token
                if (!token) {
                    continue
                }
                if(data[i].type=="IOS"){
                    isEmptyIos = false
                    ios_tokens.push(token)
                }else{
                    isEmptyAndr = false
                android_tokens.push(token)
                }


            }
            if (!isEmptyAndr) {
              pushToAndroid(android_tokens, payload)
            }
            if(!isEmptyIos) {
                pushToIosSilent(ios_tokens,payload)
            }
        }).catch(error => {
            console.error(error);
        })
    }

    this.pushToBcCarriers = function push(bcard_id,payload) {
        if(!payload.action){
            console.error("Wrong actions",payload)
        }
    db.manyOrNone("SELECT\n" +
        " user_bc_handlers.bcard_id,\n" +
        " user_bc_handlers.user_id,\n" +
        " user_bc_handlers.version_order_id,\n" +
        " user_devices.type,\n" +
        " user_devices.push_token\n" +
        "FROM\n" +
        " user_bc_handlers\n" +
        "INNER JOIN user_devices ON user_bc_handlers.user_id = user_devices.user_id where user_bc_handlers.bcard_id=${bcard_id}",
        {bcard_id: bcard_id}).then(data => {
        var android_tokens = []
        var ios_tokens = []
        var isEmptyIos = true
        var isEmptyAndr = true
        for (var i in data) {
            var token = data[i].push_token
            if (!token) {
                continue
            }
            if(data[i].type=="IOS"){
                isEmptyIos = false
                ios_tokens.push(token)
            }else{
                isEmptyAndr = false
                android_tokens.push(token)
            }
        }
        if (!isEmptyAndr) {
            pushToAndroid(android_tokens, payload)
        }
        if(!isEmptyIos) {
            pushToIosSilent(ios_tokens,payload)
        }

    }).catch(error => {
        console.error(error);
    })
}

}

module.exports = Pusher;